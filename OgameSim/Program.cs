﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using OgameSim.simulator;
using OgameSim.simulator.model;
using OgameSim.simulator.statsModel;

namespace OgameSim
{
    internal static class Program
    {
        private const string Version = "0.1";
        
        public static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            new Gui().Start(Version);
        }
    }
}