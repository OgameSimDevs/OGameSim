﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using OgameSim.simulator.model;
using OgameSim.simulator.statsModel;

namespace OgameSim
{
    public static class ApiUtil
    {
        public static string GetResultForApiId(string apiId)
        {
            
            var url = "https://nomoreangel.de/api-reader/?apiid=" + apiId + "&rawOut=on";
            var result = CallRestMethod(url);
            return IsApiCallSuccessfull(result) ? ParseResult(result) : null;
        }

        private static bool IsApiCallSuccessfull(string result)
        {
            return Regex.Match(result,
                    "<pre>stdClass\\sObject(.|\\n)*\\((.|\\n)*\\[generic\\](.|\\n)*\\[details\\](.|\\n)*\\)(.|\\n)*<\\/pre>")
                .Success;
        }

        private static string ParseResult(string rawIn)
        {
            var matches = Regex.Matches(rawIn, "(\\((?:.|\\n)*\\))(?:.|\\n)*?(?:<\\/pre>)");
            var res = "";
            foreach (Match match in matches)
            {
                res = match.Groups[1].Value;
                break;
            }

            return res;
        }

        public static bool IsDefDataEncoded(string api)
        {
            return Regex.Match(api,
                "(?:coords;(\\d+:\\d+:\\d+)\\|)((?:\\d\\d\\d;\\d+\\|?)*)\\|").Success;
        }

        public static UnitCountWrapper ParseDefUnits(string api)
        {
            var result = UnitCountWrapper.CreateEmpty();
            var match = Regex.Match(api, "(?:coords;(\\d+:\\d+:\\d+)\\|)((?:\\d\\d\\d;\\d+\\|?)*)\\|");
            var data = match.Groups[2].Value;
            var splittedData = data.Split('|');
            foreach (var singleValue in splittedData)
            {
                var tmp = singleValue.Split(';');
                if (UnitFactory.IdToUnitName.ContainsKey(Convert.ToInt32(tmp[0])))
                {
                    result.SetUnitCount(UnitFactory.IdToUnitName[Convert.ToInt32(tmp[0])], Convert.ToInt32(tmp[1]));
                }
            }

            return result;
        }

        public static Generalnformation.Tech ParseTech(string api)
        {
            var matches = Regex.Matches(api,
                "(?:coords;(\\d+:\\d+:\\d+)\\|)(?:109;(\\d+)\\|110;(\\d+)\\|111;(\\d+)\\|115;(\\d+)\\|117;(\\d+)\\|118;(\\d+))");
            var result = new Generalnformation.Tech();
            foreach (Match match in matches)
            {
                result.Damage = Convert.ToInt32(match.Groups[2].Value);
                result.Shield = Convert.ToInt32(match.Groups[3].Value);
                result.Hull = Convert.ToInt32(match.Groups[4].Value);
            }

            return result;
        }

        private static string CallRestMethod(string url)
        {
            var webrequest = (HttpWebRequest) WebRequest.Create(url);
            webrequest.Method = "GET";
            webrequest.ContentType = "text/html; charset=UTF-8";
            var webresponse = (HttpWebResponse) webrequest.GetResponse();
            var enc = Encoding.GetEncoding("utf-8");
            var responseStream = new StreamReader(webresponse.GetResponseStream(), enc);
            var result = responseStream.ReadToEnd();
            webresponse.Close();
            return result;
        }
    }
}