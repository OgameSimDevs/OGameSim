﻿using System;
using System.Collections.Generic;
using OgameSim.simulator;
using OgameSim.simulator.model;
using OgameSim.simulator.statsModel;

namespace OgameSim
{
    public class Gui
    {
        private readonly Settings _settings;

        public Gui()
        {
            _settings = new Settings
            {
                lootPercentage = 0.5d,
                percentageForDebrisField = 0.5d,
                percentageForDefDebrisField = 0d,
                tradingCourse = new TradingCourse
                {
                    MetalRatio = 2.5,
                    CrystalRatio = 1.5,
                    DeuteriumRatio = 1d
                }
            };
        }      

        private void ChangeSettings()
        {
            Util.chooseNextOpInLoop(new Dictionary<string, Util.Operation>()
            {
                {"loot", () =>
                {
                    Console.WriteLine("new loot ratio");
                    _settings.lootPercentage = Util.ReadDouble(x => x >= 0 && x <= 1);
                }},
                {"debris", () =>
                    {
                        Console.WriteLine("new ship debris ratio");
                        _settings.percentageForDebrisField = Util.ReadDouble(x => x >= 0 && x <= 1);
                    }
                },
                {"def", () =>
                    {
                        Console.WriteLine("new def debris ratio");
                        _settings.percentageForDefDebrisField = Util.ReadDouble(x => x >= 0 && x <= 1);
                    }
                },
                {"m", () =>
                    {
                        Console.WriteLine("new metal trade ratio");
                        _settings.tradingCourse.MetalRatio = Util.ReadDouble(x => x > 0);
                    }
                },
                {"c", () =>
                    {
                        Console.WriteLine("new crystal trade ratio");
                        _settings.tradingCourse.CrystalRatio = Util.ReadDouble(x => x > 0);
                    }
                },
                {"d", () =>
                    {
                        Console.WriteLine("new deuterium trade ratio");
                        _settings.tradingCourse.MetalRatio = Util.ReadDouble(x => x > 0);
                    }
                },
                {"p", () => _settings.print()}
            }, "change setting for loot, debris, def debris ratio, metall, crystal, deut trading ratio, quit or print settings (loot/debris/def/m/c/d/q/p)");
        }

        private void GenerateUnitTable()
        {
            //also print trading course
            //TODO generate table of worthness of units per 1.000.000 trade units
            //ability to write this table to a text file
            
            //add more tables with other worthness factors,...
        }

        private void simulator()
        {
            //TODO noch so zeug wie handleskurs reinstecken und daraus profit errechnen
            var simController = new SimController();
            simController.enterAttackerDate();
            simController.enterDefenderData();
            Util.chooseNextOpInLoop(new Dictionary<string, Util.Operation>
            {
                {"s", () => simController.simulate()},
                {"c", () => simController.changeSettings()},
                {"p", () => simController.printCurrentSettings()},
                {"newa", () => simController.enterAttackerDate()},
                {"newd", () => simController.enterAttackerDate()}
            }, "simulate, change units or techs, print, new attacker or defender or return (s/c/p/newa/newd/q)");
        }

        public void Start(string version)
        {
            PrintUtil.PrintSpacer('*');
            PrintUtil.PrintMiddleText("Welcome to console OGameSim Version " + version);
            PrintUtil.PrintSpacer('*');
            
           Util.chooseNextOpInLoop(new Dictionary<string, Util.Operation>
           {
               {"s", simulator},
               {"c", ChangeSettings}
           }, "new simulation, change settings, quit (s/c/q)");
        }
    }
}