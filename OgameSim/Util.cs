﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq.Expressions;
using System.Security.Cryptography.X509Certificates;
using OgameSim.simulator.statsModel;

namespace OgameSim
{
    public static class Util
    {
        
        
        public static void changeIntDictionary(Dictionary<string, int> toChange)
        {
            Console.WriteLine(" | change one of the following settings | ");
            Console.WriteLine(" V old values:                          V ");
            PrintUtil.PrintDictionary(toChange);
            var changeOps = new Dictionary<string, Operation>();
            foreach (var key in toChange.Keys)
            {
                changeOps[key] = () =>
                {
                    Console.Write(key + "'s old value: " + toChange[key] + " enter new value");
                    toChange[key] = ReadNumber(x => x >= 0);
                };
            }
            chooseNextOpInLoop(changeOps, "change setting by typing the name and hitting enter or (q) to return");
        }
        
        public static double ReadDouble(Func<double, bool> numberChecker)
        {
            while (true)
            {
                var read = Console.ReadLine();
                try
                {
                    var number = Convert.ToDouble(read);
                    if (numberChecker.Invoke(number))
                    {
                        return number;
                    }
                }
                catch (FormatException)
                {

                }
                catch (OverflowException)
                {
                    
                }
                Console.WriteLine("please enter a valid number");
            }
        }

        public static int ReadNumberWithSkip(Func<int, bool> numberChecker, int defaultValue)
        {
            while (true)
            {
                var read = Console.ReadLine();
                if (string.IsNullOrWhiteSpace(read))
                {
                    return defaultValue;
                }
                try
                {
                    var number = Convert.ToInt32(read);
                    if (numberChecker.Invoke(number))
                    {
                        return number;
                    }
                }
                catch (FormatException)
                {

                }
                catch (OverflowException)
                {
                    
                }
                Console.WriteLine("please enter a valid number");
            }
        }
  
        public static int ReadNumber(Func<int, bool> numberChecker)
        {
            while (true)
            {
                var read = Console.ReadLine();
                try
                {
                    var number = Convert.ToInt32(read);
                    if (numberChecker.Invoke(number))
                    {
                        return number;
                    }
                }
                catch (FormatException)
                {

                }
                catch (OverflowException)
                {
                    
                }
                Console.WriteLine("please enter a valid number");
            }
        }

        public delegate void Operation();

        public static void chooseNextOpInLoop(Dictionary<string, Operation> options, string description)
        {
            var quit = false;
            options["q"] = () => quit = true;
            while (!quit)
            {
                chooseNextOp(options, description);
            }
        }

        public static void chooseNextOp(Dictionary<string, Operation> options, string description)
        {
            Console.WriteLine(description);           
            while (true)
            {
                var input = Console.ReadLine();
                if (input != null && options.ContainsKey(input))
                {
                    options[input].Invoke();
                    return;
                }
                Console.WriteLine("invalid input");
            }
        }       
    }
}