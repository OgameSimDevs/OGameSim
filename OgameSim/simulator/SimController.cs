﻿using System;
using System.Collections.Generic;
using System.Linq;
using OgameSim.simulator.model;
using OgameSim.simulator.statsModel;

namespace OgameSim.simulator
{
    public class SimController
    {
        private UnitCountWrapper _attackerUnits = UnitCountWrapper.CreateEmpty();
        private UnitCountWrapper _defenderUnits = UnitCountWrapper.CreateEmpty();
        private Generalnformation.Tech _attackerTech = new Generalnformation.Tech();
        private Generalnformation.Tech _defenderTech = new Generalnformation.Tech();

        public void changeSettings()
        {
            Util.chooseNextOpInLoop(new Dictionary<string, Util.Operation>
            {
                {"au", () => Util.changeIntDictionary(_attackerUnits.UnitCount)},
                {"at", () => Util.changeIntDictionary(_attackerTech.TechValues)},
                {"du", () => Util.changeIntDictionary(_defenderUnits.UnitCount)},
                {"dt", () => Util.changeIntDictionary(_defenderTech.TechValues)}
            }, "change attacker units / tech or defenders units / tech (au/at/du/dt) or return (q)");
        }

        public void printCurrentSettings()
        {
            PrintUtil.PrintSpacer(' ');
            printSettings(_attackerUnits, _attackerTech, "attacker");
            PrintUtil.PrintSpacer(' ');
            printSettings(_defenderUnits, _defenderTech, "defender");
            PrintUtil.PrintSpacer(' ');
        }

        public void enterAttackerDate()
        {
            enterData(false, out _attackerUnits, out _attackerTech);
        }

        public void enterDefenderData()
        {
            enterData(true, out _defenderUnits, out _defenderTech);
        }

        private static void printSettings(UnitCountWrapper playerUnits, Generalnformation.Tech playerTech,
            string playerName)
        {
            PrintUtil.PrintSpacer('*');
            PrintUtil.PrintMiddleText(playerName + " settings");
            PrintUtil.PrintSpacer('*');
            PrintUtil.PrintSpacer(' ');
            PrintUtil.PrintMiddleText("Tech");
            PrintUtil.PrintDictionary(playerTech.TechValues);
            PrintUtil.PrintMiddleText("Units");
            PrintUtil.PrintDictionary(playerUnits.UnitCount);
            PrintUtil.PrintSpacer('-');
        }

        private static void enterData(bool defender, out UnitCountWrapper playersUnits,
            out Generalnformation.Tech playersTech)
        {
            PrintUtil.PrintSpacer('*');
            PrintUtil.PrintMiddleText("enter data for " + (defender ? "defender" : "attacker"));
            PrintUtil.PrintSpacer('*');
            var resultingPlayerUnits = UnitCountWrapper.CreateEmpty();
            var resultingPlayerTech = new Generalnformation.Tech();
            Util.chooseNextOp(new Dictionary<string, Util.Operation>()
            {
                {
                    "a", () =>
                    {
                        Console.WriteLine("paste api key here");
                        var apiKey = Console.ReadLine();
                        while (true)
                        {
                            if (ApiUtil.IsDefDataEncoded(apiKey))
                            {
                                var fleetWithSolars = UnitFactory.ShipNames;
                                if (defender)
                                {
                                    resultingPlayerUnits = ApiUtil.ParseDefUnits(apiKey);
                                    fleetWithSolars.Add(UnitFactory.NAME_SOLAR_SAT);
                                }

                                foreach (var unitName in fleetWithSolars)
                                {
                                    Console.WriteLine("enter amount of " + unitName);
                                    resultingPlayerUnits.UnitCount[unitName] = Util.ReadNumberWithSkip(x => x >= 0, 0);
                                }

                                resultingPlayerTech = ApiUtil.ParseTech(apiKey);
                                break;
                            }

                            var apiResult = ApiUtil.GetResultForApiId(apiKey);
                            if (apiResult != null)
                            {
                                var parser = new Parser(apiResult);
                                var generalInformation = new Generalnformation(parser);
                                resultingPlayerTech = generalInformation.TechLevel;
                                var defense = parser.ParsePair("defense_type", false);
                                var ships = parser.ParsePair("ship_type", false);
                                foreach (var unit in defense.Concat(ships))
                                {
                                    //ignore values not in dict (rockets)
                                    if (!UnitFactory.IdToUnitName.ContainsKey(unit.Key)) continue;
                                    var unitName = UnitFactory.IdToUnitName[unit.Key];
                                    //attacker can't have defense to attack with
                                    if (defender || UnitFactory.ShipNames.Contains(unitName))
                                    {
                                        resultingPlayerUnits.SetUnitCount(unitName, unit.Value);
                                    }
                                }

                                break;
                            }

                            Console.WriteLine("invalid api key");
                            apiKey = Console.ReadLine();
                        }
                    }
                },
                {
                    "m", () =>
                    {
                        Console.WriteLine("enter tech");
                        foreach (var techName in Generalnformation.Tech.TechNames)
                        {
                            Console.WriteLine("enter level of " + techName + " tech");
                            resultingPlayerTech.TechValues[techName] = Util.ReadNumberWithSkip(x => x > 0, 0);
                        }

                        var unitsToRead = UnitFactory.ShipNames;
                        if (defender)
                        {
                            unitsToRead = UnitFactory.UnitNames;
                        }

                        foreach (var unitName in unitsToRead)
                        {
                            Console.WriteLine("enter amount of " + unitName);
                            resultingPlayerUnits.UnitCount[unitName] = Util.ReadNumberWithSkip(x => x >= 0, 0);
                        }
                    }
                }
            }, "enter data via api or manually? (a/m)");
            playersUnits = resultingPlayerUnits;
            playersTech = resultingPlayerTech;
        }

        public void simulate()
        {
            var resim = true;
            while (resim)
            {
                Console.WriteLine("How mane sims?");
                var numOfSims = Util.ReadNumber(x => x > 0);
                var attackerUnitsTemplate = new PlayersUnitsWrapper(_attackerUnits, _attackerTech);
                var defenderUnitsTemplate = new PlayersUnitsWrapper(_defenderUnits, _defenderTech);

                var sims = new Simulator[numOfSims];
                for (var i = 0; i < numOfSims; i++)
                {
                    sims[i] = new Simulator(attackerUnitsTemplate.Clone(), defenderUnitsTemplate.Clone());
                    Console.WriteLine("sim " + (i + 1) + "/" + numOfSims);
                    sims[i].Simulate();
                }

                Console.WriteLine("simulated.");
                PrintUtil.CalcAndPrintResults("attacker", false, _attackerUnits,
                    sims.AsEnumerable().Select(x => x.Attacker).ToArray());
                PrintUtil.CalcAndPrintResults("defender", true, _defenderUnits,
                    sims.AsEnumerable().Select(x => x.Defender).ToArray());

                //TODO calc % recs looted, transporters needed
                //TODO calc TF and recyclers needed
                //TODO calc profit
                //TODO calc flight time

                Util.chooseNextOp(new Dictionary<string, Util.Operation>()
                {
                    {"y", () => { }},
                    {"n", () => resim = false}
                }, "resim (y/n)");
            }
        }
    }
}