﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using OgameSim.simulator.statsModel;

namespace OgameSim.simulator.model
{
    public class PlayersUnitsWrapper
    {
        public int NewSize;
        public Unit[] Units;

        public PlayersUnitsWrapper(IEnumerable<Unit> units)
        {
            Units = units.ToArray();
        }

        public PlayersUnitsWrapper Clone()
        {
            var clonedUnits = new Unit[Units.Length];
            for (var i = 0; i < Units.Length; i++)
            {
                clonedUnits[i] = Units[i].Clone();
            }            
            return new PlayersUnitsWrapper(clonedUnits);
        }

        public PlayersUnitsWrapper(UnitCountWrapper unitCounts, Generalnformation.Tech tech)
        {
            IEnumerable<Unit> tmp = new Collection<Unit>();
            var unitFactory = new UnitFactory(tech);
            foreach (var unitKeyValue in unitCounts.UnitCount)
            {
                tmp = tmp.Concat(UnitFactory.BuildUnits(unitFactory.Builders[unitKeyValue.Key].Invoke(),
                    unitKeyValue.Value));
            }
            this.Units = tmp.ToArray();

        }

        public void RemoveExplodedUnits()
        {
            var newUnits = new Unit[NewSize];
            var counter = 0;
            foreach (var unit in Units)
            {
                if (!unit.Explode)
                {
                    newUnits[counter++] = unit;
                }
            }
            this.Units = newUnits;
        }
    }
}