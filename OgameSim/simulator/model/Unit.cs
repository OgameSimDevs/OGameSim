﻿using System.Collections.Generic;

namespace OgameSim.simulator.model
{
    public class Unit
    {
        public string Name;
        private double _maxHull;
        public double MaxShield { set; private get; }
        public double Damage { set; get; }
        private bool _explode = false;
        private bool _isExploeded = false;
        private double _currentShield;
        private double _currentHull;

        public double MaxHull
        {
            private get { return _maxHull; }

            set
            {
                _maxHull = value;
                _currentHull = value;
            }
        }

        public bool Explode
        {
            get => _explode;

            private set
            {
                if (_explode || !value) return;
                _explode = true;
                _isExploeded = true;
            }
        }

        public bool IsExploeded
        {
            get
            {
                var tmp = _isExploeded;
                _isExploeded = false;
                return tmp;
            }
        }

        public Dictionary<string, int> RapidFireAgainst = new Dictionary<string, int>();

        public void ChargeShield()
        {
            _currentShield = MaxShield;
        }

        public void TankDamage(double damage)
        {
            if (Explode)
            {
                return;
            }

            var remainingDamage = damage;
            if (damage >= 0.01d * MaxShield)
            {
                if (_currentShield > remainingDamage)
                {
                    _currentShield -= remainingDamage;
                }
                else
                {
                    remainingDamage -= _currentShield;
                    _currentShield = 0;
                    _currentHull -= remainingDamage;
                    if (_currentHull < 0)
                    {
                        _currentHull = 0;
                    }
                }
            }

            var damageTaken = 1d - _currentHull / MaxHull;
            if (damageTaken >= 0.3d)
            {
                Explode = SimUtil.RandomTrueWithProbability(damageTaken);
            }
        }

        public Unit Clone()
        {
            return new Unit
            {
                Name = Name,
                MaxHull = MaxHull,
                MaxShield = MaxShield,
                Damage = Damage,
                RapidFireAgainst = RapidFireAgainst,
            };
        }
    }
}