﻿using System;
using OgameSim.simulator.statsModel;

namespace OgameSim.simulator.model
{
    public class Settings
    {
        public TradingCourse tradingCourse;
        public double percentageForDebrisField;
        public double percentageForDefDebrisField;
        public double lootPercentage;

        public double CalcTotalCost(UnitProperties unitProperties)
        {
            return unitProperties.Metal / tradingCourse.MetalRatio + unitProperties.Crystal / tradingCourse.CrystalRatio +
                   unitProperties.Deuterium / tradingCourse.DeuteriumRatio;
        }

        public void print()
        {
            PrintUtil.PrintMiddleText("current settings");
            const int length = 40;
            Console.WriteLine(PrintUtil.MakePretty("percentage of ships to debris", length ) + percentageForDebrisField);
            Console.WriteLine(PrintUtil.MakePretty("percentage of def to debris", length) +  percentageForDefDebrisField);
            Console.WriteLine(PrintUtil.MakePretty("loot percentage", length) +  lootPercentage);
            Console.WriteLine(PrintUtil.MakePretty("tarding (metall, crystal, deuterium)", length) +  tradingCourse.MetalRatio + ":" + tradingCourse.CrystalRatio + ":" + tradingCourse.DeuteriumRatio);
            PrintUtil.PrintMiddleText("end of settings");
        }
    }

    public class TradingCourse
    {
        public double MetalRatio;
        public double CrystalRatio;
        public double DeuteriumRatio;
    }

    public class UnitProperties
    {
        public int Metal;
        public int Crystal;
        public int Deuterium;
        public int Cargo;
        //TODO maybe spped, but later bc speed depends on science

        public UnitProperties(int metal, int crystal, int deuterium)
        {
            Metal = metal;
            Crystal = crystal;
            Deuterium = deuterium;
            Cargo = 0;
        }
        
        public UnitProperties(int metal, int crystal, int deuterium, int cargo)
        {
            Metal = metal;
            Crystal = crystal;
            Deuterium = deuterium;
            Cargo = cargo;
        }
    }
}