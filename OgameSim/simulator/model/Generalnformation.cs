﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OgameSim.simulator.statsModel;

namespace OgameSim.simulator.model
{
    public class Generalnformation
    {
        public const string PROP_SR_ID = "sr_id";
        public const string PROP_EVENT_TIME = "event_time";
        public const string PROP_ATTACKER_NAME = "attacker_name";
        public const string PROP_ATTACKER_PLANET_NAME = "attacker_planet_name";
        public const string PROP_ATTACKER_PLANET_COORDINATES = "attacker_planet_coordinates";
        public const string PROP_ATTACKER_PLANET_TYPE = "attacker_planet_type";
        public const string PROP_ATTACKER_ALLIANZE_NAME = "attacker_alliance_name";
        public const string PROP_ATTACKER_ALLIANZE_TAG = "attacker_alliance_tag";

        public const string PROP_DEFENDER_NAME = "defender_name";
        public const string PROP_DEFENDER_PLANET_NAME = "defender_planet_name";
        public const string PROP_DEFENDER_PLANET_COORDINATES = "defender_planet_coordinates";
        public const string PROP_DEFENDER_PLANET_TYPE = "defender_planet_type";
        public const string PROP_DEFENDER_ALLIANZE_NAME = "defender_alliance_name";
        public const string PROP_DEFENDER_ALLIANZE_TAG = "defender_alliance_tag";

        public const string PROP_FAILED_SHIPS = "failed_ships";
        public const string PROP_FAILED_DEFENSE = "failed_defense";
        public const string PROP_FAILED_BUILDINGS = "failed_buildings";
        public const string PROP_FAILED_RESEARCH = "failed_research";

        public const int PROPERTY_LENGTH = 30;


        public readonly Dictionary<string, string> Properties = new Dictionary<string, string>();

        public Tech TechLevel { get; }

        public Generalnformation(Parser parser)
        {        
            foreach (var propertyId in _propertyIds)
            {
                Properties.Add(propertyId, parser.ParseSingleValue(propertyId));
            }

            if (Properties[PROP_FAILED_RESEARCH].Equals(""))
            {
                var research = parser.ParsePair("research_type", true);
                TechLevel = new Tech();
                foreach (var singleResearch in research)
                {
                    switch (singleResearch.Key)
                    {
                        case 109:
                            TechLevel.Damage = singleResearch.Value;
                            break;
                        case 110:
                            TechLevel.Shield = singleResearch.Value;
                            break;
                        case 111:
                            TechLevel.Hull = singleResearch.Value;
                            break;
                    }
                }

                //TODO handle warnings if techs are not in the sb
            }
            else
            {
                Console.WriteLine("Failed to get tech levels from spy probe, enter them manually");
                TechLevel = new Tech();
                Console.WriteLine("Weapon Tech:");
                TechLevel.Damage = Util.ReadNumber(x => x >= 0);
                Console.WriteLine("Shield Tech:");
                TechLevel.Shield = Util.ReadNumber(x => x >= 0);
                Console.WriteLine("Hull Tech:");
                TechLevel.Hull = Util.ReadNumber(x => x >= 0);
            }
        }

        

        public class Tech
        {
            private const string PROPERTY_WEAPON_TECH = "Weapon Tech";
            private const string PROPERTY_SHIELD_TECH = "Shield Tech";
            private const string PROPERTY_HULL_TECH = "Hull Tech";

            public int Damage
            {
                get => TechValues[PROPERTY_WEAPON_TECH];
                set => TechValues[PROPERTY_WEAPON_TECH] = value;
            }

            public int Shield
            {
                get => TechValues[PROPERTY_SHIELD_TECH];
                set => TechValues[PROPERTY_SHIELD_TECH] = value;
            }

            public int Hull
            {
                get => TechValues[PROPERTY_HULL_TECH];
                set => TechValues[PROPERTY_HULL_TECH] = value;
            }
            
            public static readonly Collection<string> TechNames = new Collection<string>
            {
                PROPERTY_WEAPON_TECH,
                PROPERTY_SHIELD_TECH,
                PROPERTY_HULL_TECH
            };
            
            public readonly Dictionary<string, int> TechValues = new Dictionary<string, int>
            {
                {PROPERTY_WEAPON_TECH, 0},
                {PROPERTY_SHIELD_TECH, 0},
                {PROPERTY_HULL_TECH, 0}
            };

           
        }

        public void Print()
        {
           PrintUtil.PrintMiddleText("General Information");
            foreach (var propertyId in _propertyIds)
            {
                Console.WriteLine(PrintUtil.MakePretty(propertyId + ":",PROPERTY_LENGTH) + Properties[propertyId]);
            }

            if (TechLevel == null) return;
            PrintUtil.PrintMiddleText("Defender Combat Tech");
            Console.WriteLine(PrintUtil.MakePretty("Defender Weapon Tech Level:", PROPERTY_LENGTH) + TechLevel.Damage);
            Console.WriteLine(PrintUtil.MakePretty("Defender Shield Tech Level:", PROPERTY_LENGTH) + TechLevel.Shield);
            Console.WriteLine(PrintUtil.MakePretty("Defender Hull Tech Level:", PROPERTY_LENGTH) + TechLevel.Hull);
        }

        private readonly string[] _propertyIds = {
            PROP_SR_ID,
            PROP_EVENT_TIME,
            PROP_ATTACKER_NAME,
            PROP_ATTACKER_PLANET_NAME,
            PROP_ATTACKER_PLANET_COORDINATES,
            PROP_ATTACKER_PLANET_TYPE,
            PROP_ATTACKER_ALLIANZE_NAME,
            PROP_ATTACKER_ALLIANZE_TAG,
            PROP_DEFENDER_NAME,
            PROP_DEFENDER_PLANET_NAME,
            PROP_DEFENDER_PLANET_COORDINATES,
            PROP_DEFENDER_PLANET_TYPE,
            PROP_DEFENDER_ALLIANZE_NAME,
            PROP_DEFENDER_ALLIANZE_TAG,
            PROP_FAILED_SHIPS,
            PROP_FAILED_DEFENSE,
            PROP_FAILED_BUILDINGS,
            PROP_FAILED_RESEARCH
        };
    }
}
