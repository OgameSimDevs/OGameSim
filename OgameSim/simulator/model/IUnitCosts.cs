﻿using System.Collections.Generic;

namespace OgameSim.simulator.model
{
    public static class UnitCosts
    {
        public static UnitProperties getCostForUnit(string unitName)
        {
            return _unitCosts[unitName];
        }
        
        private static readonly Dictionary<string, UnitProperties> _unitCosts = new Dictionary<string, UnitProperties>
        {
            {UnitFactory.NAME_SPY_PROBE, new UnitProperties(0,1000,0)},
            {UnitFactory.NAME_SOLAR_SAT, new UnitProperties(0,2000,500)},
            {UnitFactory.NAME_LIGHT_TRANSPORTER, new UnitProperties(2000,2000,0, 5000)},
            {UnitFactory.NAME_HEAVY_TRANSPORTER, new UnitProperties(6000,6000,0, 25000)},
            {UnitFactory.NAME_RECYCLER, new UnitProperties(10000,6000,2000, 20000)},
            {UnitFactory.NAME_COLONY_SHIP, new UnitProperties(10000,20000,10000, 7500)},
            {UnitFactory.NAME_LIGHT_FIGHTER, new UnitProperties(3000,1000,0, 50)},
            {UnitFactory.NAME_HEAVY_FIGHTER, new UnitProperties(6000,4000,0, 100)},
            {UnitFactory.NAME_CRUISER, new UnitProperties(20000,7000,2000, 800)},
            {UnitFactory.NAME_BATTLE_SHIP, new UnitProperties(45000,15000,0, 1500)},
            {UnitFactory.NAME_BATTLE_CRUISER, new UnitProperties(30000,40000,15000, 750)},
            {UnitFactory.NAME_BOMBER, new UnitProperties(50000,25000,15000, 500)},
            {UnitFactory.NAME_DESTROYER, new UnitProperties(60000,50000,15000, 2000)},
            {UnitFactory.NAME_RIP, new UnitProperties(5000000,4000000,1000000, 1000000)},
            
            {UnitFactory.NAME_ROCKET_LAUNCHER, new UnitProperties(2000,0,0)},
            {UnitFactory.NAME_LIGHT_LASER, new UnitProperties(1500,500,0)},
            {UnitFactory.NAME_HEAVY_LASER, new UnitProperties(6000,2000,0)},
            {UnitFactory.NAME_GAUSS_CANON, new UnitProperties(20000,15000,2000)},
            {UnitFactory.NAME_ION_CANNON, new UnitProperties(2000,6000,0)},
            {UnitFactory.NAME_PLASMA_CANON, new UnitProperties(50000,50000,30000)},
            {UnitFactory.NAME_LIGHT_SHIELD, new UnitProperties(10000,10000,0)},
            {UnitFactory.NAME_HEAVY_SHIELD, new UnitProperties(50000,50000,0)}
        };
    }
}