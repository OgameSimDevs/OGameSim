﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace OgameSim.simulator.model
{
    public class UnitFactory
    {
        private readonly Generalnformation.Tech _tech;

        public UnitFactory(Generalnformation.Tech tech)
        {
            _tech = tech;
            Builders = new Dictionary<string, Func<Unit>>
            {
                {NAME_SPY_PROBE, BuildSpyProbe},
                {NAME_SOLAR_SAT, BuildSolarSat},
                {NAME_LIGHT_TRANSPORTER, BuildLightTransporter},
                {NAME_HEAVY_TRANSPORTER, BuildHeavyTransporter},
                {NAME_RECYCLER, BuildRecycler},
                {NAME_COLONY_SHIP, BuildColonyShip},
                {NAME_LIGHT_FIGHTER, BuildLightFighter},
                {NAME_HEAVY_FIGHTER, BuildHeavyFighter},
                {NAME_CRUISER, BuildCruiser},
                {NAME_BATTLE_SHIP, BuildBattleShip},
                {NAME_BATTLE_CRUISER, BuildBattleCruiser},
                {NAME_BOMBER, BuildBomber},
                {NAME_DESTROYER, BuildDestroyer},
                {NAME_RIP, BuildRip},

                {NAME_ROCKET_LAUNCHER, BuildRocketLauncher},
                {NAME_LIGHT_LASER, BuildLightLaser},
                {NAME_HEAVY_LASER, BuildHeavyLaser},
                {NAME_GAUSS_CANON, BuildGaussCannon},
                {NAME_ION_CANNON, BuildIonCannon},
                {NAME_PLASMA_CANON, BuildPlasma},
                {NAME_LIGHT_SHIELD, BuildLightShield},
                {NAME_HEAVY_SHIELD, BuildHeavyShield}
            };
        }


        public readonly Dictionary<string, Func<Unit>> Builders;


        private double CalcActualDamageValue(double baseDamage)
        {
            return baseDamage + (baseDamage * 0.01d * _tech.Damage);
        }

        private double CalcActualShieldValue(double baseShield)
        {
            return baseShield + (baseShield * 0.1d * _tech.Shield);
        }

        private double CalcActualHullValue(double baseHull)
        {
            return baseHull + (baseHull * 0.1d * _tech.Hull);
        }

        public static readonly Dictionary<int, string> IdToUnitName = new Dictionary<int, string>
        {
            {202, NAME_LIGHT_TRANSPORTER},
            {203, NAME_HEAVY_TRANSPORTER},
            {204, NAME_LIGHT_FIGHTER},
            {205, NAME_HEAVY_FIGHTER},
            {206, NAME_CRUISER},
            {207, NAME_BATTLE_SHIP},
            {208, NAME_COLONY_SHIP},
            {209, NAME_RECYCLER},
            {210, NAME_SPY_PROBE},
            {211, NAME_BOMBER},
            {212, NAME_SOLAR_SAT},
            {213, NAME_DESTROYER},
            {214, NAME_RIP},
            {215, NAME_BATTLE_CRUISER},

            {401, NAME_ROCKET_LAUNCHER},
            {402, NAME_LIGHT_LASER},
            {403, NAME_HEAVY_LASER},
            {404, NAME_GAUSS_CANON},
            {405, NAME_ION_CANNON},
            {406, NAME_PLASMA_CANON},
            {407, NAME_LIGHT_SHIELD},
            {408, NAME_HEAVY_SHIELD}
        };

        public static readonly ICollection<string> ShipNames = new List<string>
        {
            NAME_SPY_PROBE,
            //NAME_SOLAR_SAT, //because we can't attack with solar sats but we can def and it's not in the def api 
            NAME_LIGHT_TRANSPORTER,
            NAME_HEAVY_TRANSPORTER,
            NAME_RECYCLER,
            NAME_COLONY_SHIP,
            NAME_LIGHT_FIGHTER,
            NAME_HEAVY_FIGHTER,
            NAME_CRUISER,
            NAME_BATTLE_SHIP,
            NAME_BATTLE_CRUISER,
            NAME_BOMBER,
            NAME_DESTROYER,
            NAME_RIP
        };

        public static readonly ICollection<string> UnitNames = new List<string>
        {
            NAME_ROCKET_LAUNCHER,
            NAME_LIGHT_LASER,
            NAME_HEAVY_LASER,
            NAME_ION_CANNON,
            NAME_GAUSS_CANON,
            NAME_PLASMA_CANON,
            NAME_LIGHT_SHIELD,
            NAME_HEAVY_SHIELD,

            NAME_SPY_PROBE,
            NAME_SOLAR_SAT,
            NAME_LIGHT_TRANSPORTER,
            NAME_HEAVY_TRANSPORTER,
            NAME_RECYCLER,
            NAME_COLONY_SHIP,
            NAME_LIGHT_FIGHTER,
            NAME_HEAVY_FIGHTER,
            NAME_CRUISER,
            NAME_BATTLE_SHIP,
            NAME_BATTLE_CRUISER,
            NAME_BOMBER,
            NAME_DESTROYER,
            NAME_RIP
        };

        //Ships    
        public const string NAME_SPY_PROBE = "spy-probe";
        public const string NAME_SOLAR_SAT = "solar-satellite";
        public const string NAME_LIGHT_TRANSPORTER = "light-transporter";
        public const string NAME_HEAVY_TRANSPORTER = "heavy-transporter";
        public const string NAME_RECYCLER = "recycler";
        public const string NAME_COLONY_SHIP = "colony-ship";
        public const string NAME_LIGHT_FIGHTER = "light-fighter";
        public const string NAME_HEAVY_FIGHTER = "heavy-fighter";
        public const string NAME_CRUISER = "cruiser";
        public const string NAME_BATTLE_SHIP = "battle-ship";
        public const string NAME_BATTLE_CRUISER = "battle-cruiser";
        public const string NAME_BOMBER = "bomber";
        public const string NAME_DESTROYER = "destroyer";
        public const string NAME_RIP = "rip";

        //def
        public const string NAME_ROCKET_LAUNCHER = "rocket-launcher";
        public const string NAME_LIGHT_LASER = "light-laser";
        public const string NAME_HEAVY_LASER = "heavy-laser";
        public const string NAME_ION_CANNON = "ion-cannon";
        public const string NAME_GAUSS_CANON = "gauss-cannon";
        public const string NAME_PLASMA_CANON = "plasma-canon";
        public const string NAME_LIGHT_SHIELD = "light-shield";
        public const string NAME_HEAVY_SHIELD = "heavy-shield";


        public static IEnumerable<Unit> BuildUnits(Unit template, int count)
        {
            var units = new Collection<Unit>();
            for (var i = 0; i < count; i++)
            {
                units.Add(template.Clone());
            }

            return units;
        }

        public Unit BuildSolarSat()
        {
            return new Unit
            {
                Name = NAME_SOLAR_SAT,
                MaxHull = CalcActualHullValue(200),
                MaxShield = CalcActualShieldValue(1),
                Damage = CalcActualDamageValue(1)
            };
        }

        public Unit BuildSpyProbe()
        {
            return new Unit
            {
                Name = NAME_SPY_PROBE,
                MaxHull = CalcActualHullValue(100),
                MaxShield = CalcActualShieldValue(0.01d),
                Damage = CalcActualDamageValue(0.01d)
            };
        }

        public Unit BuildLightTransporter()
        {
            return new Unit
            {
                Name = NAME_LIGHT_TRANSPORTER,
                MaxHull = CalcActualHullValue(400),
                MaxShield = CalcActualShieldValue(10),
                Damage = CalcActualDamageValue(5),
                RapidFireAgainst =
                {
                    {NAME_SPY_PROBE, 5},
                    {NAME_SOLAR_SAT, 5}
                }
            };
        }

        public Unit BuildHeavyTransporter()
        {
            return new Unit
            {
                Name = NAME_HEAVY_TRANSPORTER,
                MaxHull = CalcActualHullValue(1200),
                MaxShield = CalcActualShieldValue(25),
                Damage = CalcActualDamageValue(5),
                RapidFireAgainst =
                {
                    {NAME_SPY_PROBE, 5},
                    {NAME_SOLAR_SAT, 5}
                }
            };
        }

        public Unit BuildRecycler()
        {
            return new Unit
            {
                Name = NAME_RECYCLER,
                MaxHull = CalcActualHullValue(1600),
                MaxShield = CalcActualShieldValue(10),
                Damage = CalcActualDamageValue(1),
                RapidFireAgainst =
                {
                    {NAME_SPY_PROBE, 5},
                    {NAME_SOLAR_SAT, 5}
                }
            };
        }

        public Unit BuildColonyShip()
        {
            return new Unit
            {
                Name = NAME_COLONY_SHIP,
                MaxHull = CalcActualHullValue(3000),
                MaxShield = CalcActualShieldValue(100),
                Damage = CalcActualDamageValue(50),
                RapidFireAgainst =
                {
                    {NAME_SPY_PROBE, 5},
                    {NAME_SOLAR_SAT, 5}
                }
            };
        }

        public Unit BuildLightFighter()
        {
            return new Unit
            {
                Name = NAME_LIGHT_FIGHTER,
                MaxHull = CalcActualHullValue(400),
                MaxShield = CalcActualShieldValue(10),
                Damage = CalcActualDamageValue(50),
                RapidFireAgainst =
                {
                    {NAME_SPY_PROBE, 5},
                    {NAME_SOLAR_SAT, 5}
                }
            };
        }

        public Unit BuildHeavyFighter()
        {
            return new Unit
            {
                Name = NAME_HEAVY_FIGHTER,
                MaxHull = CalcActualHullValue(1000),
                MaxShield = CalcActualShieldValue(25),
                Damage = CalcActualDamageValue(150),
                RapidFireAgainst =
                {
                    {NAME_SPY_PROBE, 5},
                    {NAME_SOLAR_SAT, 5},
                    {NAME_LIGHT_TRANSPORTER, 3}
                }
            };
        }

        public Unit BuildCruiser()
        {
            return new Unit
            {
                Name = NAME_CRUISER,
                MaxHull = CalcActualHullValue(2700),
                MaxShield = CalcActualShieldValue(50),
                Damage = CalcActualDamageValue(400),
                RapidFireAgainst =
                {
                    {NAME_SPY_PROBE, 5},
                    {NAME_SOLAR_SAT, 5},
                    {NAME_LIGHT_FIGHTER, 6},
                    {NAME_ROCKET_LAUNCHER, 10}
                }
            };
        }

        public Unit BuildBattleShip()
        {
            return new Unit
            {
                Name = NAME_BATTLE_SHIP,
                MaxHull = CalcActualHullValue(6000),
                MaxShield = CalcActualShieldValue(200),
                Damage = CalcActualDamageValue(1000),
                RapidFireAgainst =
                {
                    {NAME_SPY_PROBE, 5},
                    {NAME_SOLAR_SAT, 5}
                }
            };
        }

        public Unit BuildBattleCruiser()
        {
            return new Unit
            {
                Name = NAME_BATTLE_CRUISER,
                MaxHull = CalcActualHullValue(7000),
                MaxShield = CalcActualShieldValue(400),
                Damage = CalcActualDamageValue(700),
                RapidFireAgainst =
                {
                    {NAME_SPY_PROBE, 5},
                    {NAME_SOLAR_SAT, 5},
                    {NAME_LIGHT_TRANSPORTER, 3},
                    {NAME_HEAVY_TRANSPORTER, 3},
                    {NAME_HEAVY_FIGHTER, 4},
                    {NAME_CRUISER, 4},
                    {NAME_BATTLE_SHIP, 7},
                }
            };
        }

        public Unit BuildBomber()
        {
            return new Unit
            {
                Name = NAME_BOMBER,
                MaxHull = CalcActualHullValue(7500),
                MaxShield = CalcActualShieldValue(500),
                Damage = CalcActualDamageValue(1000),
                RapidFireAgainst =
                {
                    {NAME_SPY_PROBE, 5},
                    {NAME_SOLAR_SAT, 5},
                    {NAME_ROCKET_LAUNCHER, 20},
                    {NAME_LIGHT_LASER, 20},
                    {NAME_HEAVY_LASER, 10},
                    {NAME_ION_CANNON, 10}
                }
            };
        }

        public Unit BuildDestroyer()
        {
            return new Unit
            {
                Name = NAME_DESTROYER,
                MaxHull = CalcActualHullValue(11000),
                MaxShield = CalcActualShieldValue(500),
                Damage = CalcActualDamageValue(2000),
                RapidFireAgainst =
                {
                    {NAME_SPY_PROBE, 5},
                    {NAME_SOLAR_SAT, 5},
                    {NAME_BATTLE_CRUISER, 2},
                    {NAME_LIGHT_LASER, 10}
                }
            };
        }

        public Unit BuildRip()
        {
            return new Unit
            {
                Name = NAME_RIP,
                MaxHull = CalcActualHullValue(900000),
                MaxShield = CalcActualShieldValue(50000),
                Damage = CalcActualDamageValue(200000),
                RapidFireAgainst =
                {
                    //ships
                    {NAME_SPY_PROBE, 1250},
                    {NAME_SOLAR_SAT, 1250},
                    {NAME_LIGHT_TRANSPORTER, 250},
                    {NAME_HEAVY_TRANSPORTER, 250},
                    {NAME_RECYCLER, 250},
                    {NAME_COLONY_SHIP, 250},
                    {NAME_LIGHT_FIGHTER, 200},
                    {NAME_HEAVY_FIGHTER, 200},
                    {NAME_CRUISER, 33},
                    {NAME_BATTLE_SHIP, 30},
                    {NAME_BOMBER, 25},
                    {NAME_BATTLE_CRUISER, 15},
                    {NAME_DESTROYER, 5},
                    //def
                    {NAME_ROCKET_LAUNCHER, 200},
                    {NAME_LIGHT_LASER, 200},
                    {NAME_HEAVY_LASER, 100},
                    {NAME_ION_CANNON, 100},
                    {NAME_GAUSS_CANON, 50}
                }
            };
        }

        public Unit BuildRocketLauncher()
        {
            return new Unit
            {
                Name = NAME_ROCKET_LAUNCHER,
                MaxHull = CalcActualHullValue(200),
                MaxShield = CalcActualShieldValue(20),
                Damage = CalcActualDamageValue(80)
            };
        }

        public Unit BuildLightLaser()
        {
            return new Unit
            {
                Name = NAME_LIGHT_LASER,
                MaxHull = CalcActualHullValue(200),
                MaxShield = CalcActualShieldValue(25),
                Damage = CalcActualDamageValue(100)
            };
        }

        public Unit BuildHeavyLaser()
        {
            return new Unit
            {
                Name = NAME_HEAVY_LASER,
                MaxHull = CalcActualHullValue(800),
                MaxShield = CalcActualShieldValue(100),
                Damage = CalcActualDamageValue(250)
            };
        }

        public Unit BuildIonCannon()
        {
            return new Unit
            {
                Name = NAME_ION_CANNON,
                MaxHull = CalcActualHullValue(800),
                MaxShield = CalcActualShieldValue(500),
                Damage = CalcActualDamageValue(150)
            };
        }

        public Unit BuildGaussCannon()
        {
            return new Unit
            {
                Name = NAME_GAUSS_CANON,
                MaxHull = CalcActualHullValue(3500),
                MaxShield = CalcActualShieldValue(200),
                Damage = CalcActualDamageValue(1100)
            };
        }

        public Unit BuildPlasma()
        {
            return new Unit
            {
                Name = NAME_PLASMA_CANON,
                MaxHull = CalcActualHullValue(10000),
                MaxShield = CalcActualShieldValue(300),
                Damage = CalcActualDamageValue(3000)
            };
        }

        public Unit BuildLightShield()
        {
            return new Unit
            {
                Name = NAME_LIGHT_SHIELD,
                MaxHull = CalcActualHullValue(2000),
                MaxShield = CalcActualShieldValue(2000),
                Damage = CalcActualDamageValue(1)
            };
        }

        public Unit BuildHeavyShield()
        {
            return new Unit
            {
                Name = NAME_HEAVY_SHIELD,
                MaxHull = CalcActualHullValue(10000),
                MaxShield = CalcActualShieldValue(10000),
                Damage = CalcActualDamageValue(1)
            };
        }
    }
}