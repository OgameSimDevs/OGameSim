﻿using System;
using System.Collections.Generic;
using System.Linq;
using OgameSim.simulator.model;

namespace OgameSim.simulator.statsModel
{
    public class Calculator
    {
        private readonly UnitCountWrapper[] _simResults;

        public Calculator(UnitCountWrapper[] simResults)
        {
            _simResults = simResults;
        }

        public Dictionary<string, SingleUnitResult> AggregatedResults { get; } = new Dictionary<string, SingleUnitResult>();

        public void calc()
        {
            foreach (var unitName in UnitFactory.UnitNames)
            {
                var resultValues = new int[_simResults.Length];
                for (var i = 0; i < _simResults.Length; i++)
                {
                    resultValues[i] = _simResults[i].GetUnitCount(unitName);
                }

                Array.Sort(resultValues);
                double median;
                if (resultValues.Length % 2 == 0)
                {
                    var middle = resultValues.Length / 2;
                    median = (resultValues[middle] + resultValues[middle + 1]) / 2d;
                }
                else
                {
                    median = resultValues[resultValues.Length / 2];
                }

                var average = resultValues.Average();
                var singleUnitResult = new SingleUnitResult()
                {
                    Min = resultValues[0],
                    Max = resultValues[resultValues.Length - 1],
                    Average = average,
                    Median = median,
                    Varianz = calcVariance(resultValues, average)
                };
                AggregatedResults[unitName] = singleUnitResult;
            }
        }

        private static double calcVariance(IReadOnlyCollection<int> values, double average)
        {
            return Math.Sqrt(values.AsEnumerable().Select(value => (value - average) * (value - average)).Sum() /
                             values.Count);
        }
    }
}