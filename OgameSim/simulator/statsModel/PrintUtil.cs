﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography.X509Certificates;
using OgameSim.simulator.model;

namespace OgameSim.simulator.statsModel
{
    public static class PrintUtil
    {
        public static void CalcAndPrintResults(string playerName,bool printDef, UnitCountWrapper before, IEnumerable<PlayersUnitsWrapper> after)
        {
            var calculator = new Calculator(after.AsEnumerable().Select(UnitCountWrapper.CountUnits).ToArray());
            calculator.calc();
            new ResultPrinter(before, calculator.AggregatedResults).print(playerName, printDef);
        }
        
        public static void PrintDictionary(Dictionary<string, int> toPrint)
        {
            foreach (var name in toPrint.Keys)
            {
                Console.WriteLine(MakePretty(name, 30) + ": " + toPrint[name]);
            }
        }

        public static void PrintMiddleText(string text)
        {
            Console.WriteLine(MakeToMiddle(text, '-'));
        }

        public static void PrintSpacer(char spaceChar)
        {
            Console.WriteLine(new string(spaceChar, Console.WindowWidth - 1));
        }

        private static string MakeToMiddle(string text, char c)
        {
            var consoleWidth = Console.WindowWidth;
            var toPrint = " " + text + " ";
            var numOfLeftChars = (consoleWidth - toPrint.Length) / 2;
            var numOfRightChars = consoleWidth - numOfLeftChars - toPrint.Length;
            return new string(c, numOfLeftChars) + toPrint + new string(c, numOfRightChars - 1);
        }

        public static string MakePretty(int uglyNumber, int countLength)
        {
            return MakePretty(uglyNumber.ToString(), countLength);
        }

        public static string MakePretty(double uglyNumber, int countLength)
        {
            return MakePretty($"{uglyNumber:0.0}", countLength);
        }

        public static string MakePretty(string ugly, int length)
        {
            if (length <= ugly.Length)
            {
                return ugly;
            }
            return ugly + new string(' ', length - ugly.Length);
        }
    }
    
    
}