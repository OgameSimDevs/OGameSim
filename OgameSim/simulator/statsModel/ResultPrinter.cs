﻿using System;
using System.Collections.Generic;
using System.Globalization;
using OgameSim.simulator.model;

namespace OgameSim.simulator.statsModel
{
    public class ResultPrinter
    {
        private readonly Dictionary<string, SingleUnitResult> _results;
        private readonly UnitCountWrapper _before;

        public ResultPrinter(UnitCountWrapper before, Dictionary<string, SingleUnitResult> results)
        {
            this._before = before;
            this._results = results;
        }

        public void print(string player, bool printDef)
        {
            PrintUtil.PrintMiddleText(player);
            PrintUtil.PrintMiddleText("unit losses");
            var unitNames = printDef ? UnitFactory.UnitNames : UnitFactory.ShipNames;
            foreach (var unitName in unitNames)
            {
                const int nameLength = 20;
                const int countLength = 10;
                var tmp = _results[unitName];
                Console.WriteLine(PrintUtil.MakePretty(unitName, nameLength) + ": " +
                                  "count: " + PrintUtil.MakePretty(_before.GetUnitCount(unitName), countLength) +
                                  " => avg: " + PrintUtil.MakePretty(tmp.Average, countLength) + 
                                  " => avg loss: " + PrintUtil.MakePretty(_before.GetUnitCount(unitName) - tmp.Average, countLength) +
                                  " min: " + PrintUtil.MakePretty(tmp.Min, countLength) +
                                  " median: " + PrintUtil.MakePretty(tmp.Median, countLength) + 
                                  " max: " + PrintUtil.MakePretty(tmp.Max,countLength) + 
                                  " min max dist: " + PrintUtil.MakePretty(tmp.Max - tmp.Min,countLength) +
                                  " var: " + $"{tmp.Varianz:0.0}");
            }

            PrintUtil.PrintMiddleText("end of unit losses");
        }

        
    }
}