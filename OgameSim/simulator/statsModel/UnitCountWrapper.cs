﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;
using OgameSim.simulator.model;

namespace OgameSim.simulator.statsModel
{
    public class UnitCountWrapper
    {
        public readonly Dictionary<string, int> UnitCount = new Dictionary<string, int>();
        
        

        public int GetUnitCount(string unitName)
        {
            return UnitCount.ContainsKey(unitName) ? UnitCount[unitName] : 0;
        }

        public void SetUnitCount(string unitName, int value)
        {
            UnitCount[unitName] = value;
        }

        public static UnitCountWrapper CreateEmpty()
        {
            return new UnitCountWrapper();
        }

        private UnitCountWrapper()
        {
        }

        public static UnitCountWrapper CountUnits(PlayersUnitsWrapper playersUnitsWrapper)
        {
            var result = CreateEmpty();
            foreach (var unit in playersUnitsWrapper.Units)
            {
                result.UnitCount[unit.Name] = result.GetUnitCount(unit.Name) + 1;
            }
            return result;
        }
    } 
}