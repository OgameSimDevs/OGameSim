﻿namespace OgameSim.simulator.statsModel
{
    public class SingleUnitResult
    {
        public int Min;
        public int Max;
        public double Average;
        public double Median;
        public double Varianz;
    }
}