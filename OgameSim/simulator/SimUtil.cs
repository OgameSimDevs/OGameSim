﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OgameSim.simulator.model;

namespace OgameSim.simulator
{
    public static class SimUtil
    {
        private static readonly Random RandomInstance = new Random();

        private static int Roll(int from, int to)
        {
            return RandomInstance.Next(from, to);
        }

        public static int PickShip(int shipCount)
        {
            return Roll(0, shipCount - 1);
        }

        public static bool RandomTrueWithProbability(double probability)
        {
            var randomNumber = RandomInstance.NextDouble(); //between 0 and 0.99999999999999978
            return randomNumber <= probability;
        }

        public static bool ShootAgain(int rapidFire)
        {
            return rapidFire > 1 && RandomTrueWithProbability((rapidFire - 1d) / rapidFire);
        }

        public static int GetRapidFireOrDefault(Dictionary<string, int> rapidFireValues, string nameOfShip)
        {
            return rapidFireValues.ContainsKey(nameOfShip) ? rapidFireValues[nameOfShip] : 1;
        }
    }
}