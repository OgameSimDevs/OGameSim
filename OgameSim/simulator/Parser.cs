﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using OgameSim.simulator.model;

namespace OgameSim.simulator
{
    public class Parser
    {
        private readonly string _toParse;

        public Parser(string toParse)
        {
            this._toParse = toParse;
        }

        public string ParseSingleValue(string type)
        {
            var pattern = "(?:\\[" + Regex.Escape(type) + "\\]\\s\\=\\>\\s)([\\w]*)";
            var result = "";
            var matches = Regex.Matches(_toParse, pattern);
            foreach (Match match in matches)
            {
                result = match.Groups[1].Value;
                break;
            }

            return result;
        }

        public IEnumerable<KeyValuePair<int, int>> ParsePair(string type, bool level)
        {
            var countOrLevel = level ? "level" : "count";
            var pattern = "(?:\\[" + Regex.Escape(type) +  "]\\s\\=\\>\\s)([0-9]+)(?:[\\s]*\\["+ countOrLevel +"]\\s\\=\\>\\s)([0-9]+)";
            var matches = Regex.Matches(_toParse, pattern);
            var result = new Collection<KeyValuePair<int, int>>();
            foreach (Match match in matches)
            {
                result.Add(new KeyValuePair<int, int>(Convert.ToInt32(match.Groups[1].Value), Convert.ToInt32(match.Groups[2].Value)));
            }

            return result;
        }
    }
}
