﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OgameSim.simulator.model;

namespace OgameSim.simulator
{
    public class Simulator
    {
        public readonly PlayersUnitsWrapper Attacker;
        public readonly PlayersUnitsWrapper Defender;
        private const int MAX_ROUNDS = 6;
        public Simulator(PlayersUnitsWrapper attacker, PlayersUnitsWrapper defender)
        {
            this.Attacker = attacker;
            this.Defender = defender;
        }

        private static void SimTurn(PlayersUnitsWrapper shooter, PlayersUnitsWrapper tanker)
        {
            var newSize = tanker.Units.Length;
            foreach (var attackerShip in shooter.Units)
            {
                var fireAgain = true;
                while (fireAgain)
                {
                    var shipToDamage = SimUtil.PickShip(tanker.Units.Length);
                    var unitToDamage = tanker.Units[shipToDamage];
                    unitToDamage.TankDamage(attackerShip.Damage);
                    if (unitToDamage.IsExploeded)
                    {
                        newSize--;
                    }
                    fireAgain = SimUtil.ShootAgain(SimUtil.GetRapidFireOrDefault(attackerShip.RapidFireAgainst, unitToDamage.Name));
                }
            }

            tanker.NewSize = newSize;
        }
        


        public void Simulate()
        {
            for (var i = 0; i < MAX_ROUNDS; i++)
            {
                if (Attacker.Units.Length == 0 || Defender.Units.Length == 0)
                {
                    break;
                }
                Parallel.ForEach(Attacker.Units, (unit) => { unit.ChargeShield(); });
                Parallel.ForEach(Defender.Units, (unit) => { unit.ChargeShield(); });
                SimTurn(Attacker, Defender);
                SimTurn(Defender, Attacker);
                Attacker.RemoveExplodedUnits();
                Defender.RemoveExplodedUnits();
            }
        }

        private static void PrintRemainingUnits(PlayersUnitsWrapper player)
        {
            Console.WriteLine("all: " + player.Units.Length);
            var attRes = new Dictionary<string, int>();
            foreach (var ship in player.Units)
            {
                if (attRes.ContainsKey(ship.Name))
                {
                    attRes[ship.Name]++;
                }
                else
                {
                    attRes[ship.Name] = 1;
                }
            }

            foreach (var shipResult in attRes)
            {
                Console.WriteLine(shipResult.Key + ": " + shipResult.Value);
            }
        }

        public void Print()
        {
            Console.WriteLine("- - - Simualtion Results - - -");
            Console.WriteLine("Winner: " + (Attacker.Units.Length == 0 ? "Defender Wins" : Defender.Units.Length == 0 ? "Attacker Wins" : "Draw"));
            Console.WriteLine("- - - Attacker Remaining Ships - - -");
            PrintRemainingUnits(Attacker);
            Console.WriteLine("- - - Defender Remaining Ships - - -");
            PrintRemainingUnits(Defender);
        }
    }
}